/* global describe, it, expect */

import Vue from 'vue'
import Labels from 'src/components/Labels'

describe('Labels.vue', () => {
  const vm = new Vue({
    template: '<div><labels></labels></div>',
    components: { Labels }
  }).$mount()

  describe('data() method:', () => {
    it('should exist', () => {
      expect(typeof Labels.data).toBe('function')
    })

    it('should return labels property as a object', () => {
      expect(typeof vm.$children[0].labels).toBe('object')
    })

    it('should have a \'newLabelName\' property as a string', () => {
      expect(typeof vm.$children[0].newLabelName).toBe('string')
    })

    it('should have a \'newLabelColor\' property as a string', () => {
      expect(typeof vm.$children[0].newLabelColor).toBe('string')
    })
  })

  describe('created() method:', () => {
    it('should exist', () => {
      expect(typeof Labels.created).toBe('function')
    })
  })

  describe('methods object:', () => {
    it('should be a object', () => {
      expect(typeof Labels.methods).toBe('object')
    })

    it('should have a \'saveLabels\' method', () => {
      expect(typeof vm.$children[0].saveLabels).toBe('function')
    })

    it('should have a \'saveNewLabel\' method', () => {
      expect(typeof vm.$children[0].saveNewLabel).toBe('function')
    })

    it('should have a \'dispatchSelectedLabel\' method', () => {
      expect(typeof vm.$children[0].dispatchSelectedLabel).toBe('function')
    })

    describe('Component method: saveNewLabel', () => {
      it('should return false if newLabelName and newLabelColor are empty', () => {
        expect(vm.$children[0].saveNewLabel()).toBeFalse
      })

      it('should return false when newLabelColor is not set', () => {
        vm.$children[0].newLabelName = 'test'
        expect(vm.$children[0].saveNewLabel()).toBeFalse
      })

      it('should return false when newLabelName is not set', () => {
        vm.$children[0].newLabelColor = '#FFF'
        expect(vm.$children[0].saveNewLabel()).toBeFalse
      })

      it('should return true when newLabelName and newLabelColor are set', () => {
        vm.$children[0].newLabelName = 'test'
        vm.$children[0].newLabelColor = '#FFF'

        expect(vm.$children[0].saveNewLabel()).toBeTrue
      })

      it('should store the new label in the labels property of the data method', () => {
        vm.$children[0].labels = []

        vm.$children[0].newLabelName = 'test'
        vm.$children[0].newLabelColor = '#FFF'
        vm.$children[0].saveNewLabel()
        expect(vm.$children[0].labels.length).toBe(1)

        vm.$children[0].newLabelName = 'test 2'
        vm.$children[0].newLabelColor = '#000'
        vm.$children[0].saveNewLabel()
        expect(vm.$children[0].labels.length).toBe(2)
      })

      describe('labels should match the porper data structure', () => {
        it('should update the labels data with the correct object', () => {
          expect(vm.$children[0].labels[0].name).toBe('test')
          expect(vm.$children[0].labels[0].color).toBe('#FFF')
          expect(vm.$children[0].labels[0].active).toBeFalse

          expect(vm.$children[0].labels[1].name).toBe('test 2')
          expect(vm.$children[0].labels[1].color).toBe('#000')
          expect(vm.$children[0].labels[1].active).toBeFalse
        })
      })
    })
  })
})
