/* global describe, it, expect */

import Vue from 'vue'
import Items from 'src/components/Items'

describe('Items.vue', () => {
  const vm = new Vue({
    template: '<div><items v-bind:filters="[0,1,2,3]"></items></div>',
    components: { Items }
  }).$mount()

  it('should render correct contents', () => {
    expect(vm.$el.querySelector('div#base')).toBeTruthy()
    expect(vm.$el.querySelector('div#base button').textContent).toBe('New item')
    expect(vm.$el.querySelector('div#base h3').textContent).toBe('Add new item')
  })

  it('should load \'model\' component', () => {
    expect(typeof Items.components).toBe('object')
  })

  describe('data() method', () => {
    it('should exists', () => {
      expect(typeof Items.data).toBe('function')
    })

    it('Should return a \'newItem\' property as a object', () => {
      expect(typeof vm.$children[0].newItem).toBe('object')
    })

    describe('newItem object in the \'data\' method', () => {
      it('should have correct properties', () => {
        expect(typeof vm.$children[0].newItem.title).toBe('string')
        expect(typeof vm.$children[0].newItem.text).toBe('string')
        expect(typeof vm.$children[0].newItem.labels).toBe('object')
        expect(typeof vm.$children[0].newItem.weight).toBe('number')
      })
    })

    it('should return a \'showAddItem\' property as a Boolean', () => {
      expect(typeof vm.$children[0].showAddItem).toBe('boolean')
    })

    it('should return a \'items\' property as a Object', () => {
      expect(typeof vm.$children[0].items).toBe('object')
    })
  })

  describe('init() method', () => {
    it('should exists', () => {
      expect(typeof Items.init).toBe('function')
    })
  })

  describe('created() method', () => {
    it('should exists', () => {
      expect(typeof Items.created).toBe('function')
    })
  })

  describe('props object', () => {
    it('should be a object', () => {
      expect(typeof Items.props).toBe('object')
    })

    it('should have a filter object', () => {
      expect(typeof Items.props.filters).toBe('object')
    })
  })

  describe('methods object', () => {
    it('should have a methods object', () => {
      expect(typeof Items.methods).toBe('object')
    })

    it('should have a \'fireSelectedItem\' method', () => {
      expect(typeof vm.$children[0].fireSelectedItem).toBe('function')
    })

    it('should have a \'addLabel\' method', () => {
      expect(typeof vm.$children[0].addLabel).toBe('function')
    })

    it('should have a \'saveItem\' method', () => {
      expect(typeof vm.$children[0].saveItem).toBe('function')
    })

    it('should have a \'activeLabels\' method', () => {
      expect(typeof vm.$children[0].activeLabels).toBe('function')
    })
  })
})
