/* global describe, it, expect */

import Vue from 'vue'
import Modal from 'src/widgets/modal'

describe('widgets/modal.vue', () => {
  const vm = new Vue({
    template: '<div><modal :show.sync="show"></modal></div>',
    components: { Modal },
    data: {
      show: true
    }
  }).$mount()

  it('should render correct contents', () => {
    expect(vm.$el.querySelector('div')).toBeTruthy()
    expect(vm.$el.querySelector('div div.modal-mask')).toBeTruthy()
    expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper')).toBeTruthy()
    expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper div.modal-container')).toBeTruthy()
  })

  describe('Should have the correct slots', () => {
    it('should have a modal-header slot', () => {
      expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper div.modal-container div.modal-header')).toBeTruthy()
      expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper div.modal-container div.modal-header').textContent.trim()).toBe('default header')
    })

    it('should have a modal-body slot', () => {
      expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper div.modal-container div.modal-body')).toBeTruthy()
      expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper div.modal-container div.modal-body').textContent.trim()).toBe('default body')
    })

    it('should have a modal-footer slot', () => {
      expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper div.modal-container div.modal-footer')).toBeTruthy()
      expect(vm.$el.querySelector('div div.modal-mask div.modal-wrapper div.modal-container div.modal-footer').textContent.trim()).toBe('default footer')
    })
  })
})
